let reading1 = "Success may lie in other people's hands, the questioner is advised to be still, responsive and receptive to advice.";
let reading2 = "Be patient and things will work out. Don't make a move until you are certain the time is right, and even then proceed with caution. Think long and hard before getting involved in something new: you could come to regret it later, for there may be things of which you are unaware - important things.";
let reading3 = "That pressure will build up and may become intolerable, but that if you stand up for what you believe in, things will turn out to your advantage.";
let reading4 = "You have been battling against the odds for so long that now is the time to take a break. Do this and the answer to your problems will come to you. So hold on: the future is not as black as it seems.";
let reading5 = "There appears to be more than one option open to you. If you use logic and common sense, the decision you come to will enable you to enter that new cycle brimful with confidence.";
let reading6 = "Need for positive thinking and careful strategy. Determination brings victory.";
let reading7 = "Things are probably looking hard, but stay calm, composed and as silent as possible and you will cope, although you may have to dig deep into your reserves and be very determined to do so.";
let reading8 = "Remember that problems are usually of a temporary nature, so be ready for the time when they vanish.";
let reading9 = "Don't allow negative thoughts, worries or inhibitions to obstruct your progress.";
let reading10 = "This is a time of good fortune. If you act wisely you will benefit from it.";
let readings = [reading1,reading2,reading3,reading4,reading5,reading6,reading7,reading8,reading9,reading10];

let tarotCard = document.getElementsByClassName("tarot");
let reading = document.getElementById("readings");
for(let x=0; x < 5; x++){
tarotCard[x].addEventListener('click', function(){
	let readingNum = Math.floor(Math.random() * 10);
	setTimeout(function(){ 
		document.getElementById("tarotRow").style.display = "none";
		document.getElementById("readingRow").style.display = "flex";
	 }, 1000);
	
	reading.textContent = readings[readingNum];
	});
}

let retry = document.getElementById("retry-btn");
retry.addEventListener('click', function(){
	document.getElementById("tarotRow").style.display = "block";
	document.getElementById("readingRow").style.display = "none";
})